%%start = [14.017012 49.47550]
%endCurrentVector = [[14.0169267 49.4756741],
%    [14.0170849 49.4757147],
%    [14.0171983 49.4756906],
%    [14.0172495 49.4756725],
%    [14.0172763 49.4756615]]
%endExpectedVector = [[14.0165148 49.4761787],
%    [14.0170845 49.4761292],
%    [14.0173759 49.4760776],
%    [14.0175178 49.4760207],
%    [14.0175677 49.4759815]]

start = [14.017012 49.475503]
endCurrentVector = [[14.0170524 49.4756287],
    [14.0171873 49.4756646],
    [14.0172651 49.4756654],
    [14.0172716 49.4756644],
    [14.0172715 49.4756649]]
endExpectedVector = [[14.0165130 49.4761779],
    [14.0170812 49.4761239],
    [14.0173715 49.4760696],
    [14.0175096 49.4760162],
    [14.0175516 49.4759800]]


d1km = []
d2km = []
finalVector = []

plot(start(1), start(2),'*')
hold on
grid on

distance = []
startv = []

for c = 1:length(endExpectedVector)
    lenghtVector = sqrt((endCurrentVector(c,1) - start(1))^2 + (endCurrentVector(c,2) - start(2))^2)
    expectedVector = [endExpectedVector(c,1)-start(1) endExpectedVector(c,2)-start(2)]

    finalVector = [finalVector,
        ((expectedVector/sqrt(expectedVector(1)^2+expectedVector(2)^2))*lenghtVector+start)]

    latlon1 = finalVector(c,:)
    latlon2 = endCurrentVector(c,:)

    plot(finalVector(c,1), finalVector(c,2),'*')
    plot(endCurrentVector(c,1), endCurrentVector(c,2),'o')
    plot(endExpectedVector(c,1), endExpectedVector(c,2),'x')
    
    startv = [startv quiver(start(1),start(2), endExpectedVector(c,1) - start(1), endExpectedVector(c,2) - start(2))]
    distance = [distance quiver(finalVector(c,1),finalVector(c,2), endCurrentVector(c,1) - finalVector(c,1), endCurrentVector(c,2) - finalVector(c,2))]
    
    radius=6371;
    lat1=latlon1(1)*pi/180;
    lat2=latlon2(1)*pi/180;
    lon1=latlon1(2)*pi/180;
    lon2=latlon2(2)*pi/180;
    deltaLat=lat2-lat1;
    deltaLon=lon2-lon1;
    a=sin((deltaLat)/2)^2 + cos(lat1)*cos(lat2) * sin(deltaLon/2)^2;
    c=2*atan2(sqrt(a),sqrt(1-a));
    d1km=[d1km radius*c];    %Haversine distance

    x=deltaLon*cos((lat1+lat2)/2);
    y=deltaLat;
    d2km=[d2km radius*sqrt(x*x + y*y)];
end

legend([startv distance], 'A vector','B vector','C vector','D vector','E vector','A distance','B distance','C distance','D distance','E distance')