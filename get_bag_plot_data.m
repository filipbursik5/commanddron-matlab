bag = rosbag('_2018-04-18-10-35-21.bag');
bSel = select(bag,'Topic','/uav1/mavros/global_position/global');
msgStructs = readMessages(bSel,'DataFormat','struct');
xPoints2 = cellfun(@(m) double(m.Latitude),msgStructs);
yPoints2 = cellfun(@(m) double(m.Longitude),msgStructs);
sec2 = cellfun(@(m) double(m.Header.Stamp.Sec),msgStructs);

plot(yPoints2,xPoints2);
hold on;