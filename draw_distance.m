hold on;

grid on;
point = []

for i=1:3638
    
    index = 1
    
    for j=1:4234
        if sec2(j,1) == sec(i,1)
            index = j            
            break
        end
    end  
    
    if index == 1
        continue
    end
    
    latlng = [xPoints(i ,1) yPoints(i, 1)]
    latlng2 = [xPoints2(index ,1) yPoints2(index, 1)]
    
    d1km = distance('rh', [xPoints(i ,1)  yPoints(i, 1)], [xPoints2(index ,1)  yPoints2(index, 1)])
    
    point = [point, d1km * 100000]
end

time = 0 : 0.01 : (length(point) - 1) / 100;
plot(time, point)

hold off